function validarNombre(){
    let inputNombre = document.getElementById("formulario-registro-nombre");
    let inputNombreMessage = document.getElementById("formulario-registro-nombre-message");
    if(inputNombre.value.length < 2)
    {
        inputNombre.className = "contacto-formulario-invalid";
        inputNombreMessage.className = "status-show status-invalid";
        inputNombreMessage.innerHTML = "Nombre inválido (mínimo 2 caracteres)";
        return false;
    }
    else
    {
        inputNombre.className = "contacto-formulario-valid";
        inputNombreMessage.className = "status-show status-valid";
        inputNombreMessage.innerHTML = "Nombre válido";
        return true;
    }
}
function validarEmail(){
    let inputEmail = document.getElementById("formulario-registro-email");
    let inputEmailMessage = document.getElementById("formulario-registro-email-message");
    try{
        let afterarroba = inputEmail.value.split("@")[1];
        let posicionPunto = afterarroba.lastIndexOf(".")
        let len = afterarroba.length;
        if(posicionPunto < 0 || posicionPunto >= len-2 || posicionPunto < len-5){
            inputEmail.setAttribute("class","contacto-formulario-invalid");
            inputEmailMessage.className = "status-show status-invalid";
            inputEmailMessage.innerHTML = "Email inválido";
            return false;
        }
        else{
            inputEmail.setAttribute("class","contacto-formulario-valid");
            inputEmailMessage.className = "status-show status-valid";
            inputEmailMessage.innerHTML = "Email válido";
            return true;
        }    
    }
    catch{
        return false;
    }
    finally{
        
    }
}
function validarCiudad(){
    let inputCiudad = document.getElementById("formulario-registro-ciudad");
    let inputCiudadMessage = document.getElementById("formulario-registro-ciudad-message");
    if(inputCiudad.value == "none"){
        inputCiudad.setAttribute("class","contacto-formulario-invalid");
        inputCiudadMessage.className = "status-show status-invalid";
        inputCiudadMessage.innerHTML = "Escoja una ciudad";
        return false;
    }
    else{
        inputCiudad.setAttribute("class","contacto-formulario-valid");
        inputCiudadMessage.className = "status-noshow";
        inputCiudadMessage.innerHTML = "";
        return true;
    }
}

function validarContrasena(){
    let inputContrasena = document.getElementById("formulario-registro-contrasena");
    let inputConfContra = document.getElementById("formulario-registro-confContra");
    if(inputConfContra.value != inputContrasena.value)
    {
        inputContrasena.className="status-invalid";
        inputConfContra.className="status-invalid";
        return false;
    }
    else
    {
        inputContrasena.className="status-valid";
        inputConfContra.className="status-valid";
        return true;
    }

}

function validarAlEnviar(){
    try{
        let resultado = validarNombre() && validarEmail() && validarCiudad() && validarContrasena();
        return resultado;
    }
    catch(ex){//si salta la excepción
        console.error(ex);
    }
    finally{//siempre se ejecuta
        //return false;
    }    
}

window.onload = function(){
    document.getElementById("formulario-registro-nombre").onblur = validarNombre;
    document.getElementById("formulario-registro-nombre").onkeyup = validarNombre;
    document.getElementById("formulario-registro-email").onblur = validarEmail;
    document.getElementById("formulario-registro-ciudad").onblur = validarCiudad;
    document.getElementById("formulario-registro-confContra").onblur = validarContrasena;
    document.forms.registro.onsubmit = validarAlEnviar;
}