function validarNombre(){
    let inputNombre = document.getElementById("formulario-cambio-nombre");
    if(inputNombre.value.length < 2)
    {
        inputNombre.className = "contacto-formulario-invalid";
        return false;
    }
    else
    {
        inputNombre.className = "contacto-formulario-valid";
        return true;
    }
}
function validarEmail(){
    let inputEmail = document.getElementById("formulario-cambio-email");
    try{
        let afterarroba = inputEmail.value.split("@")[1];
        let posicionPunto = afterarroba.lastIndexOf(".")
        let len = afterarroba.length;
        if(posicionPunto < 0 || posicionPunto >= len-2 || posicionPunto < len-5){
            inputEmail.setAttribute("class","contacto-formulario-invalid");
            return false;
        }
        else{
            inputEmail.setAttribute("class","contacto-formulario-valid");
            return true;
        }    
    }
    catch{
        return false;
    }
    finally{
        
    }
}
function validarCiudad(){
    let inputCiudad = document.getElementById("formulario-cambio-ciudad");
    if(inputCiudad.value == "none"){
        inputCiudad.setAttribute("class","contacto-formulario-invalid");
        return false;
    }
    else{
        inputCiudad.setAttribute("class","contacto-formulario-valid");
        return true;
    }
}

function validarContrasena(){
    let inputContrasena = document.getElementById("formulario-cambio-contrasena");
    let inputConfContra = document.getElementById("formulario-cambio-confContra");
    if(inputConfContra.value != inputContrasena.value)
    {
        inputContrasena.className="contacto-formulario-invalid";
        inputConfContra.className="contacto-formulario-invalid";
        return false;
    }
    else
    {
        inputContrasena.className="contacto-formulario-valid";
        inputConfContra.className="contacto-formulario-valid";
        return true;
    }

}

function validarAlEnviar(){
    try{
        let resultado = validarNombre() && validarEmail() && validarCiudad() && validarContrasena();
        return resultado;
    }
    catch(ex){//si salta la excepción
        console.error(ex);
    }
    finally{//siempre se ejecuta
        //return false;
    }    
}

window.onload = function(){
    document.getElementById("formulario-cambio-nombre").onblur = validarNombre;
    document.getElementById("formulario-cambio-nombre").onkeyup = validarNombre;
    document.getElementById("formulario-cambio-email").onblur = validarEmail;
    document.getElementById("formulario-cambio-ciudad").onblur = validarCiudad;
    document.getElementById("formulario-cambio-confContra").onblur = validarContrasena;
    document.forms.cambio.onsubmit = validarAlEnviar;
}