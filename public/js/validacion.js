function validarNombre(){
    let inputNombre = document.getElementById("contacto-formulario-nombre");
    let inputNombreMessage = document.getElementById("contacto-formulario-nombre-message");
    if(inputNombre.value.length < 2)
    {
        inputNombre.className = "contacto-formulario-invalid";
        inputNombreMessage.className = "status-show status-invalid";
        inputNombreMessage.innerHTML = "Nombre inválido (mínimo 2 caracteres)";
        return false;
    }
    else
    {
        inputNombre.className = "contacto-formulario-valid";
        inputNombreMessage.className = "status-show status-valid";
        inputNombreMessage.innerHTML = "Nombre válido";
        return true;
    }
}
function validarEmail(){
    let inputEmail = document.getElementById("contacto-formulario-email");
    let inputEmailMessage = document.getElementById("contacto-formulario-email-message");
    try{
        let afterarroba = inputEmail.value.split("@")[1];
        let posicionPunto = afterarroba.lastIndexOf(".")
        let len = afterarroba.length;
        if(posicionPunto < 0 || posicionPunto >= len-2 || posicionPunto < len-5){
            inputEmail.setAttribute("class","contacto-formulario-invalid");
            inputEmailMessage.className = "status-show status-invalid";
            inputEmailMessage.innerHTML = "Email inválido";
            return false;
        }
        else{
            inputEmail.setAttribute("class","contacto-formulario-valid");
            inputEmailMessage.className = "status-show status-valid";
            inputEmailMessage.innerHTML = "Email válido";
            return true;
        }    
    }
    catch{

    }
    finally{
        return false;
    }

}

function validarCiudad(){
    let inputCiudad = document.getElementById("contacto-formulario-ciudad");
    let inputCiudadMessage = document.getElementById("contacto-formulario-ciudad-message");
    if(inputCiudad.value == "none"){
        inputCiudad.setAttribute("class","contacto-formulario-invalid");
        inputCiudadMessage.className = "status-show status-invalid";
        inputCiudadMessage.innerHTML = "Escoja una ciudad";
    }
    else{
        inputCiudad.setAttribute("class","contacto-formulario-valid");
        inputCiudadMessage.className = "status-noshow";
        inputCiudadMessage.innerHTML = "";
    }
}

function validarCodigoPostal(){
    let inputCP = document.getElementById("contacto-formulario-cp");
    let inputCPMessage = document.getElementById("contacto-formulario-cp-message");
    let CodigoPostal = parseInt(inputCP.value);
    if(CodigoPostal<1000 || CodigoPostal > 52999 || inputCP.value==""){
        inputCP.className = "contacto-formulario-invalid";
        inputCPMessage.className = "status-show status-invalid";
        inputCPMessage.innerHTML = "Código postal incorrecto";
    }
    else{
        inputCP.className = "contacto-formulario-valid";
        inputCPMessage.className = "status-noshow";
        inputCPMessage.innerHTML = "Código postal admitido";
    }
}

function validarTelefono(){
    let inputTlfn = document.getElementById("contacto-formulario-telefono");
    let inputTlfnMessage = document.getElementById("contacto-formulario-telefono-message");
    let objExpReg = new RegExp("^[0-9]{6,9}$");
    if(!objExpReg.test(inputTlfn.value)){
        inputTlfn.className = "contacto-formulario-invalid";
        inputTlfnMessage.className = "status-show status-invalid";
        inputTlfnMessage.innerHTML = "Teléfono incorrecto";
    }
    else{
        inputTlfn.className = "contacto-formulario-valid";
        inputTlfnMessage.className = "status-noshow";
        inputTlfnMessage.innerHTML = "";
    }
}

function validarEdad(){
    let inputEdad = document.getElementById("contacto-formulario-edad");
    let inputEdadMessage = document.getElementById("contacto-formulario-edad-message");
    let edad = parseInt(inputEdad.value);
    if(edad<18 || inputEdad.value==""){
        inputEdad.className = "contacto-formulario-invalid";
        inputEdadMessage.className = "status-show status-invalid";
        inputEdadMessage.innerHTML = "No eres un adulto";
    }
    else if(edad>150){
        inputEdad.className = "contacto-formulario-invalid";
        inputEdadMessage.className = "status-show status-invalid";
        inputEdadMessage.innerHTML = "Llama a Records Guinness";
    }
    else{
        inputEdad.className = "contacto-formulario-valid";
        inputEdadMessage.className = "status-noshow";
        inputEdadMessage.innerHTML = "";
    }
}

function validarAlEnviar(){
    try{
        let resultado = validarNombre() && validarEmail() && validarCiudad() && validarCodigoPostal() && validarTelefono() && validarEdad();
        return resultado;
    }
    catch(ex){//si salta la excepción
        console.error(ex);
    }
    finally{//siempre se ejecuta
        //return false;
    }    
}

window.onload = function(){
    document.forms.registro.onsubmit = validarAlEnviar;
    document.getElementById("contacto-formulario-nombre").onblur = validarNombre;
    document.getElementById("contacto-formulario-nombre").onkeyup = validarNombre;
    document.getElementById("contacto-formulario-email").onblur = validarEmail;
    document.getElementById("contacto-formulario-email").onkeyup = validarEmail;
    document.getElementById("contacto-formulario-ciudad").onblur = validarCiudad;
    document.getElementById("contacto-formulario-cp").onblur = validarCodigoPostal;
    document.getElementById("contacto-formulario-telefono").onblur = validarTelefono;
    document.getElementById("contacto-formulario-edad").onblur = validarEdad;

    let selectExt = document.getElementById("extensionTelefono");
    let extPais = [{valor: "+34", texto: "España (+34)"},
        {valor: "+1", texto: "Estados Unidos (+1)"},
        {valor: "+32", texto: "Portugal (+32)"},
        {valor: "+33", texto: "Italia (+33)"}];
    let extFrancia = new Object();
    extFrancia.valor = "+33";
    extFrancia.texto = "Francia (+33)";
    extPais.push(extFrancia);

    for(let i = 0; i <extPais.length; i++){
        let option = document.createElement("option");
        option.value = extPais[i].valor;
        option.innerHTML = extPais[i].texto;
        selectExt.appendChild(option);
    }
}

