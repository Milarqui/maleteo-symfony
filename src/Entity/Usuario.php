<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UsuarioRepository")
 */
class Usuario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\Column(type="integer")
     */
    private $Ciudad;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Guardian;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Viajero;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Contraseña;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): self
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getCiudad(): ?int
    {
        return $this->Ciudad;
    }

    public function setCiudad(int $Ciudad): self
    {
        $this->Ciudad = $Ciudad;

        return $this;
    }

    public function getGuardian(): ?bool
    {
        return $this->Guardian;
    }

    public function setGuardian(bool $Guardian): self
    {
        $this->Guardian = $Guardian;

        return $this;
    }

    public function getViajero(): ?bool
    {
        return $this->Viajero;
    }

    public function setViajero(bool $Viajero): self
    {
        $this->Viajero = $Viajero;

        return $this;
    }

    public function getContraseña(): ?string
    {
        return $this->Contraseña;
    }

    public function setContraseña(string $Contraseña): self
    {
        $this->Contraseña = $Contraseña;

        return $this;
    }
}
