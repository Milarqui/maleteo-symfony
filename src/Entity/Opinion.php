<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OpinionRepository")
 */
class Opinion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Viajero;

    /**
     * @ORM\Column(type="integer")
     */
    private $Guardian;

    /**
     * @ORM\Column(type="text")
     */
    private $Texto;

    /**
     * @ORM\Column(type="date")
     */
    private $Fecha;

    /**
     * @ORM\Column(type="integer")
     */
    private $Ciudad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajero(): ?int
    {
        return $this->Viajero;
    }

    public function setViajero(int $Viajero): self
    {
        $this->Viajero = $Viajero;

        return $this;
    }

    public function getGuardian(): ?int
    {
        return $this->Guardian;
    }

    public function setGuardian(int $Guardian): self
    {
        $this->Guardian = $Guardian;

        return $this;
    }

    public function getTexto(): ?string
    {
        return $this->Texto;
    }

    public function setTexto(string $Texto): self
    {
        $this->Texto = $Texto;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->Fecha;
    }

    public function setFecha(\DateTimeInterface $Fecha): self
    {
        $this->Fecha = $Fecha;

        return $this;
    }

    public function getCiudad(): ?int
    {
        return $this->Ciudad;
    }

    public function setCiudad(int $Ciudad): self
    {
        $this->Ciudad = $Ciudad;

        return $this;
    }
}
