<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MaleteroRepository")
 */
class Maletero
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Viajero;

    /**
     * @ORM\Column(type="integer")
     */
    private $Guardian;

    /**
     * @ORM\Column(type="integer")
     */
    private $Ciudad;

    /**
     * @ORM\Column(type="datetime")
     */
    private $FechaInicio;

    /**
     * @ORM\Column(type="datetime")
     */
    private $FechaFin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getViajero(): ?int
    {
        return $this->Viajero;
    }

    public function setViajero(int $Viajero): self
    {
        $this->Viajero = $Viajero;

        return $this;
    }

    public function getGuardian(): ?int
    {
        return $this->Guardian;
    }

    public function setGuardian(int $Guardian): self
    {
        $this->Guardian = $Guardian;

        return $this;
    }

    public function getCiudad(): ?int
    {
        return $this->Ciudad;
    }

    public function setCiudad(int $Ciudad): self
    {
        $this->Ciudad = $Ciudad;

        return $this;
    }

    public function getFechaInicio(): ?\DateTimeInterface
    {
        return $this->FechaInicio;
    }

    public function setFechaInicio(\DateTimeInterface $FechaInicio): self
    {
        $this->FechaInicio = $FechaInicio;

        return $this;
    }

    public function getFechaFin(): ?\DateTimeInterface
    {
        return $this->FechaFin;
    }

    public function setFechaFin(\DateTimeInterface $FechaFin): self
    {
        $this->FechaFin = $FechaFin;

        return $this;
    }
}
