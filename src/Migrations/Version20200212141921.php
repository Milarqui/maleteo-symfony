<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200212141921 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demo DROP FOREIGN KEY demo_ibfk_1');
        $this->addSql('ALTER TABLE demo DROP FOREIGN KEY demo_ibfk_2');
        $this->addSql('DROP INDEX ciudad ON demo');
        $this->addSql('ALTER TABLE demo CHANGE ciudad ciudad INT NOT NULL');
        $this->addSql('ALTER TABLE maletero DROP FOREIGN KEY maletero_ibfk_1');
        $this->addSql('ALTER TABLE maletero DROP FOREIGN KEY maletero_ibfk_2');
        $this->addSql('ALTER TABLE maletero DROP FOREIGN KEY maletero_ibfk_3');
        $this->addSql('DROP INDEX ciudad ON maletero');
        $this->addSql('DROP INDEX guardian ON maletero');
        $this->addSql('DROP INDEX viajero ON maletero');
        $this->addSql('ALTER TABLE opinion DROP FOREIGN KEY opinion_ibfk_1');
        $this->addSql('ALTER TABLE opinion DROP FOREIGN KEY opinion_ibfk_2');
        $this->addSql('ALTER TABLE opinion DROP FOREIGN KEY opinion_ibfk_3');
        $this->addSql('DROP INDEX ciudad ON opinion');
        $this->addSql('DROP INDEX guardian ON opinion');
        $this->addSql('DROP INDEX viajero ON opinion');
        $this->addSql('ALTER TABLE usuario DROP FOREIGN KEY usuario_ibfk_1');
        $this->addSql('DROP INDEX ciudad ON usuario');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demo CHANGE ciudad ciudad INT DEFAULT NULL');
        $this->addSql('ALTER TABLE demo ADD CONSTRAINT demo_ibfk_1 FOREIGN KEY (ciudad) REFERENCES ciudad (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE demo ADD CONSTRAINT demo_ibfk_2 FOREIGN KEY (ciudad) REFERENCES ciudad (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX ciudad ON demo (ciudad)');
        $this->addSql('ALTER TABLE maletero ADD CONSTRAINT maletero_ibfk_1 FOREIGN KEY (ciudad) REFERENCES ciudad (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE maletero ADD CONSTRAINT maletero_ibfk_2 FOREIGN KEY (guardian) REFERENCES usuario (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE maletero ADD CONSTRAINT maletero_ibfk_3 FOREIGN KEY (viajero) REFERENCES usuario (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX ciudad ON maletero (ciudad)');
        $this->addSql('CREATE INDEX guardian ON maletero (guardian)');
        $this->addSql('CREATE INDEX viajero ON maletero (viajero)');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT opinion_ibfk_1 FOREIGN KEY (ciudad) REFERENCES ciudad (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT opinion_ibfk_2 FOREIGN KEY (guardian) REFERENCES usuario (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE opinion ADD CONSTRAINT opinion_ibfk_3 FOREIGN KEY (viajero) REFERENCES usuario (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX ciudad ON opinion (ciudad)');
        $this->addSql('CREATE INDEX guardian ON opinion (guardian)');
        $this->addSql('CREATE INDEX viajero ON opinion (viajero)');
        $this->addSql('ALTER TABLE usuario ADD CONSTRAINT usuario_ibfk_1 FOREIGN KEY (ciudad) REFERENCES ciudad (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX ciudad ON usuario (ciudad)');
    }
}
