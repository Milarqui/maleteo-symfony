<?php

namespace App\Repository;

use App\Entity\Maletero;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Maletero|null find($id, $lockMode = null, $lockVersion = null)
 * @method Maletero|null findOneBy(array $criteria, array $orderBy = null)
 * @method Maletero[]    findAll()
 * @method Maletero[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaleteroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Maletero::class);
    }

    // /**
    //  * @return Maletero[] Returns an array of Maletero objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Maletero
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
