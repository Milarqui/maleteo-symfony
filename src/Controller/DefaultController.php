<?php

namespace App\Controller;

use App\Entity\Ciudad;
use App\Entity\Demo;
use App\Entity\Maletero;
use App\Entity\Opinion;
use App\Entity\Usuario;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/",name="main")
     */
    public function main(EntityManagerInterface $em){
        $opiniones = $em->getRepository(Opinion::class)->findAll();
        $regPas = $this->allPaises($em);
        $ciudades = $regPas["ciudades"];
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        $muestra = [];
        if(count($opiniones) <= 3){
            foreach($opiniones as $op){
                $idCiudad = $op->getCiudad();
                $ciudad = $em->getRepository(Ciudad::class)->findOneBy(["id"=>$idCiudad]);
                $pais = $ciudad->getPais();
                $region = $ciudad->getRegion();
                $city = $ciudad->getCiudad();

                $texto = $op->getTexto();
                
                $idViajero = $op->getViajero();
                $viajero = $em->getRepository(Usuario::class)->findOneBy(["id"=>$idViajero])->getNombre();
                $citta = "$city, $region, $pais";
                $m = ["usuario"=>$viajero,"texto"=>$texto,"ciudad"=>$citta];
                $muestra[] = $m;
            }
        }
        else{
            $claves = array_rand($opiniones,3);
            for ($i=0; $i<3; $i++)
            {
                $op = $opiniones[$claves[$i]];
                $idCiudad = $op->getCiudad();
                $ciudad = $em->getRepository(Ciudad::class)->findOneBy(["id"=>$idCiudad]);
                $pais = $ciudad->getPais();
                $region = $ciudad->getRegion();
                $city = $ciudad->getCiudad();

                $texto = $op->getTexto();
                
                $idViajero = $op->getViajero();
                $viajero = $em->getRepository(Usuario::class)->findOneBy(["id"=>$idViajero])->getNombre();
                $citta = "$city, $region, $pais";
                $m = ["usuario"=>$viajero,"texto"=>$texto,"ciudad"=>$citta];
                $muestra[] = $m;
            }    
        }
        return $this->render("main.html.twig",
        ["opiniones"=>$muestra,
        "ciudades"=>$ciudades,
        "regiones"=>$regiones,
        "paises"=>$paises,
        "usuario"=>[]]);
    }

    /**
     * @Route("/registro",name="registro")
     */
    public function demo(Request $request, EntityManagerInterface $em)
    {
        $regPas = $this->allPaises($em);
        $ciudades = $regPas["ciudades"];
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        $user = $request->request->get("usuario");
        $email = $request->request->get("email");
        $country = $request->request->get("pais");
        $region = $request->request->get("region");
        $city = $request->request->get("ciudad");
        
        $ciudad = $em->getRepository(Ciudad::class)->findOneBy(["Ciudad"=>$city,"Region"=>$region,"Pais"=>$country]);
        $id = $ciudad->getId();
        $usuario = ["usuario"=>$user,"email"=>$email,"ciudad"=>$city,"region"=>$region,"pais"=>$country];
            
        $newDemo = new Demo();
        $newDemo->setUsuario($user);
        $newDemo->setEmail($email);
        $newDemo->setCiudad($id);
    
        $em->persist($newDemo);
        $em->flush();    
        
        $datos = ['usuario'=>$usuario,
        "ciudades"=>$ciudades,
        "regiones"=>$regiones,
        "paises"=>$paises];
        return $this->render("registro.html.twig",$datos);
    }

    /**
     * @Route("/registrado",name="registrado")
     */
    public function registro(Request $request, EntityManagerInterface $em)
    {
        $user = $request->request->get("usuario");
        $email = $request->request->get("email");
        $country = $request->request->get("pais");
        $region = $request->request->get("region");
        $city = $request->request->get("ciudad");
        $contraseña = $request->request->get("contraseña");
        $viajero =  $request->request->get("viajero");
        $guardian =  $request->request->get("guardian");

        $ciudad = $em->getRepository(Ciudad::class)->findOneBy(["Ciudad"=>$city,"Region"=>$region,"Pais"=>$country]);
        $id = $ciudad->getId();
        $usuario = $em->getRepository(Usuario::class)->findOneBy(["Nombre"=>$user]);
        
        $regPas = $this->allPaises($em);
        $ciudades = $em->getRepository(Ciudad::class);
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        if($usuario==null)
        {
            $newUsuario = new Usuario();
            $newUsuario->setNombre($user);
            $newUsuario->setCiudad($id);
            $newUsuario->setEmail($email);
            $newUsuario->setContraseña($contraseña);
            $newUsuario->setViajero($viajero);
            $newUsuario->setGuardian($guardian);
    
            $em->persist($newUsuario);
            $em->flush();
    
            return $this->redirect("/");    
        }
        else
        {
            $usuario = ["nombre"=>$user,
            "email"=>$email,
            "ciudad"=>$city,
            "region"=>$region,
            "pais"=>$country,
            "ciudades"=>$ciudades,
            "regiones"=>$regiones,
            "paises"=>$paises];
            return $this->render("registro.html.twig",$usuario);
        }
    }
    /**
     * @Route("/perfil-c",name="perfil-c")
     */
    public function cambioPerfil(Request $request, EntityManagerInterface $em)
    {
        $user = $request->request->get("usuario");
        $email = $request->request->get("email");
        $pais = $request->request->get("pais");
        $region = $request->request->get("region");
        $city = $request->request->get("ciudad");
        $contraseña = $request->request->get("contraseña");
        $confContra = $request->request->get("confContra");
        $viajero =  $request->request->get("viajero");
        $guardian =  $request->request->get("guardian");

        $regPas = $this->allPaises($em);
        $ciudades = $em->getRepository(Ciudad::class);
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        if($contraseña == $confContra){
            $usuario = $em->getRepository(Usuario::class)->findOneBy(["Nombre"=>$user]);
            $usuario->setEmail($email);
            $value = $em->getRepository(Ciudad::class)->findOneBy(["Ciudad"=>$city,"Region"=>$region,"Pais"=>$pais]);
            $id = $value->getId();
            $ciudad = "$city,$region,$pais";
            $usuario->setCiudad($id);
            $usuario->setGuardian($guardian);
            $usuario->setViajero($viajero);
            $v = $usuario->getViajero() == 1 ? "Sí" : "No";
            $g = $usuario->getGuardian() == 1 ? "Sí" : "No";
            $us = [
                "usuario"=>$user,
                "email"=>$email,
                "ciudad"=>$ciudad,
                "region"=>$region,
                "pais"=>$pais,
                "viajero"=>$v,
                "guardian"=>$g,
                "ciudades"=>$ciudades,
                "regiones"=>$regiones,
                "paises"=>$paises];
            return $this->render("perfil.html.twig",$us);
        }
    }

    /**
     * @Route("/login",name="login")
     */
    public function login(Request $request, EntityManagerInterface $em)
    {
        $user = $request->request->get("user");
        $password = $request->request->get("password");

        $usuario = $em->getRepository(Usuario::class)->findOneBy(["Nombre"=>$user]);
        $contraseña = $usuario->getContraseña();

        $regPas = $this->allPaises($em);
        $ciudades = $regPas["ciudades"];
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        if($password == $contraseña){
            $v = $usuario->getViajero() == 1 ? "Sí" : "No";
            $g = $usuario->getGuardian() == 1 ? "Sí" : "No";
            $id = $usuario->getCiudad();
            $ciudad = $em->getRepository(Ciudad::class)->findOneBy(["id"=>$id]);
            $us = [
                "usuario"=>$user,
                "email"=>$usuario->getEmail(),
                "ciudad"=>$ciudad->getCiudad(),
                "region"=>$ciudad->getRegion(),
                "pais"=>$ciudad->getPais(),
                "viajero"=>$v,
                "guardian"=>$g,
                "ciudades"=>$ciudades,
                "regiones"=>$regiones,
                "paises"=>$paises];
            return $this->render("perfil.html.twig",$us);
        }
        return $this->redirect("/");
    }

    /**
     * @Route("/opinar",name="opinar")
     */
    public function opinar(Request $request,EntityManagerInterface $em)
    {
        $regPas = $this->allPaises($em);
        $ciudades = $regPas["ciudades"];
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        $usuarios = $em->getRepository(Usuario::class)->findAll();
        
        return $this->render("opinion.html.twig",["ciudades"=>$ciudades,"regiones"=>$regiones,"paises"=>$paises,"usuarios"=>$usuarios]);
    }

    /**
     * @Route("/opinion",name="opinion")
     */
    public function opinion(Request $request, EntityManagerInterface $em)
    {
        $user = $request->request->get("usuario");
        $guardian = $request->request->get("guardian");
        $idCiudad = $request->request->get("ciudad");
        $opinion = $request->request->get("opinion");
        $fecha = $request->request->get("fecha");

        
        $idViajero = $em->getRepository(Usuario::class)->findOneBy(["Nombre"=>$user])->getId();
        $newOpinion = new Opinion();
        $newOpinion->setViajero($idViajero);
        $newOpinion->setTexto($opinion);
        $newOpinion->setGuardian($guardian);
        $newOpinion->setCiudad($idCiudad);
        $date = new DateTime($fecha);
        $newOpinion->setFecha($date);

        $em->persist($newOpinion);
        $em->flush();
        
        return $this->redirect("/");
    }

    /**
     * @Route("/maletero",name="maletero")
     */
    public function maletero(EntityManagerInterface $em){
        $regPas = $this->allPaises($em);
        $usuarios = $em->getRepository(Usuario::class)->findAll();
        $ciudades = $regPas["ciudades"];
        $regiones = $regPas["regiones"];
        $paises = $regPas["paises"];
        
        return $this->render("maletero.html.twig",["usuarios"=>$usuarios,"ciudades"=>$ciudades,"regiones"=>$regiones,"paises"=>$paises]);    
    }
    /**
     * @Route("/reserva",name="reserva")
     */
    public function reserva(Request $request,EntityManagerInterface $em){
        $guardian = $request->request->get("guardian");
        echo $guardian;
        $usuario = $request->request->get("viajero");
        $ciudad = $request->request->get("ciudad");
        $fechaInicio = $request->request->get("fechaInicio");
        $fechaFin = $request->request->get("fechaFin");

        $viajero = $em->getRepository(Usuario::class)->findOneBy(["Nombre"=>$usuario])->getId();

        $dateIni = new DateTime($fechaInicio);
        $dateFin = new DateTime($fechaFin);

        $newUso = new Maletero();
        $newUso->setGuardian($guardian);
        $newUso->setViajero($viajero);
        $newUso->setCiudad($ciudad);
        $newUso->setFechaInicio($dateIni);
        $newUso->setFechaFin($dateFin);

        $em->persist($newUso);
        $em->flush();

        return $this->redirect("/");
    }    

    private function allPaises(EntityManagerInterface $em){
        $res = [];
        $ciudades = $em->getRepository(Ciudad::class)->findAll();
        $res["ciudades"] = $ciudades;
        $regiones = [];
        $paises = [];
        foreach($ciudades as $ciudad){
            $pais = $ciudad->getPais();
            $region = ["region"=>$ciudad->getRegion(),"pais"=>$pais];
            if(!in_array($pais,$paises))
            {
                $paises[] = $pais;
            }
            if(!in_array($region,$regiones))
            {
                $regiones[] = $region;
            }
        }
        $res["regiones"] = $regiones;
        $res["paises"] = $paises;
        return $res;
    }
}